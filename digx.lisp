(in-package :cl-getx)

(defgeneric digx (place &rest indicators)
  (:documentation "Returns the value of an property of a hierarchical data place. 
Default implementation is for plists.

By using digx instead of accessing, place values directly the user has the 
opportunity to change the underlying structure of the hierarchical place without 
having to rewrite a lot of code."))

(defun dig-down (place indicators)
  (let* ((indicator (pop indicators))
	 (next-place (if indicator
			 (getx place indicator))))
    (if indicators
	(dig-down next-place indicators)
	next-place)))

(defun set-dig-down (place indicators value)
  (let* ((indicator (pop indicators))
	 (next-place (if indicator
			 (getx place indicator))))  
    (if indicators	
	(setf (getx place indicator) 
	      (set-dig-down next-place indicators value))
	(setf (getx place indicator) value))
    place))

(defgeneric digx (place &rest indicators)
  (:documentation "Hierarchical getx. You dont really need to 
create specializers based on the type of place because
digx uses getx internally and getx does the type specific work.

Examples:

> (digx (list :eish (list :huh 1)) :eish :huh)
1

> (let ((x (list :eish (list :huh 1)))) 
    (setf (cl-getx:digx x :eish :huh) 'huh) 
    x)
(:EISH (:HUH HUH))

"))

(defmethod digx (place &rest indicators)
  (dig-down place indicators))

(defgeneric (setf digx) (value place &rest indicators))

(defmethod (setf digx) (value place &rest indicators)
  ;;TODO: When digx setf is called I cant use apply because &rest params causes extra list wrapper
  ;;find a solution
  (if (and (listp indicators) (listp (first indicators)))
      (set-dig-down place (first indicators) value)
      (set-dig-down place indicators value)))
