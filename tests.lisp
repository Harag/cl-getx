(defpackage :cl-getx-tests (:use :cl :cl-getx))
(in-package :cl-getx)

(defparameter *tests* '())

(defmacro run-test (name test-fn)
  (let ((name% (gensym))
	(test-fn% (gensym))
	(test-result% (gensym))
	(info% (gensym)))
    
    `(let ((,name% ,name)
	   (,test-fn% ,test-fn))
       
       (multiple-value-bind (,test-result% ,info%)
	   (funcall ,test-fn%)
	 (if ,test-result%
	     (push (list ,name% T) *tests*)
	     (push (list ,name% nil ,info%) *tests*))))))


(run-test "getx plist with KEYWORD key"	  
	  (lambda ()
	    (let ((result (getx (list :eish 1) :eish)))
	      (values (equal result 1) result))))

(run-test "(setf getx) plist with KEYWORD key"	  
	  (lambda ()
	    (let ((plist (list :eish 1))
		  (result ))

	      (setf (getx plist :eish) 2)
	      (setf result (getx plist :eish))
	      
	      (values (equal result 2) result))))

(run-test "getx plist with SYMBOL key"
 (lambda ()
   (let ((result (getx (list 'eish 1) 'eish)))
     (values (equal result 1) result))))

(run-test "(setf getx) plist with SYMBOL key"
 (lambda ()
   (let ((plist (list 'eish 1))
	 (result))     
     (setf (getx plist 'eish) 2)
     (setf result (getx plist 'eish))
     (values (equal result 2) result))))


(run-test "getx function accessor on plist"
 (lambda ()
   (let ((result (getx (list 'eish 1)
		       #'(lambda (place)
			   (getf place 'eish)))))
     (values (equal result 1) result))))


(run-test "(setf getx) function accessor on plist"
 (lambda ()
   (let ((plist (list 'eish 1))
	 (result))
     (setf (getx plist #'(lambda (place value)
			   (setf (getf place 'eish) value)))
	   2)
     (setf result (getx plist #'(lambda (place)
				  (getf place 'eish))))
     
     (values (equal result 2) result))))


(run-test "getx object accessor"
 (lambda ()
   (let ((x)
	 (result ))

     (defclass thingy ()
       ((some-slot :initarg :some-slot :accessor some-slot)))
     
     (setf x (make-instance 'thingy :some-slot 1))
     (setf result (getx x 'some-slot))
     
     (values (equal result 1) result))))


(run-test "(setf getx) object accessor"
 (lambda ()
   (let ((x)
	 (result ))

     (defclass thingy ()
       ((some-slot :initarg :some-slot :accessor some-slot)))
     
     (setf x (make-instance 'thingy :some-slot 1))
     (setf (getx x 'some-slot) 2)
     (setf result (getx x 'some-slot))
     
     (values (equal result 2) result))))


(run-test "(setf digx) digx with mixed type accessors"
	  (lambda ()
	    (let ((plist (list :eish (list 'huh 1))))
	      (setf (cl-getx:digx plist :eish 'huh) 'huh)
	      (values (equal plist (list :eish (list 'huh 'huh))) plist))))

(run-test "(setf digx) digx with seriously mixed type accessors"
	  (lambda ()
	    (let ((plist (list :eish (list 'huh (list :erm 1)))))

	      (setf (cl-getx:digx plist :eish 'huh #'(lambda (place &optional value)
						       (setf (getf place ':erm) value)))
		    'huh)
	      (values (equal plist (list :eish (list 'huh (list :erm 'huh)))) plist))))



(pprint *tests*)
